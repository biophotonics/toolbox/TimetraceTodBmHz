function [f, PS, PSD] = TimetraceTodBmHz(t_inputvec,A_inputvec)
%% Make time trace equidistantly spaced
f_s = 1/min(diff(t_inputvec)); % [Hz] Sampling rate
if max(diff(t_inputvec)) ~= 1/f_s
    A = interp1(t_inputvec,A_inputvec,min(t_inputvec):1/f_s:max(t_inputvec))';
else
    A = A_inputvec;
end
Length = length(A); % "Length" is the number of samples in time domain [-]
% "A" is the signal, the instantaneous voltage measurement in time domain [V]

%% Choose windowing function and termination resistance
% windowfunction = ones(1,Length); % Rectangular window
windowfunction = flattopwin(Length); % Flat top window
% windowfunction = blackman(Length); % Blackman window

R_T = 50; % [Ohms] Termination resistance

%% Calculations
ENBW = enbw(windowfunction); % [-] Equivalent noise bandwidth measured in "bins"
df = f_s/Length; % [Hz] Frequency spacing
f = (0:ceil(Length/2))*df; % [Hz] Array of frequencies in the spectrum
Vamplspectwosided = abs(fft(A.*windowfunction/sum(windowfunction))); % [V] Two-sided voltage amplitude spectrum
Vamplspec = Vamplspectwosided(1:(ceil(Length/2)+1));
Vamplspec(2:end-1) = 2*Vamplspec(2:end-1); % [V] One-sided voltage amplitude spectrum

Vrmsspec = Vamplspec; % [V] Root mean square voltage spectrum
Vrmsspec(2:end) = Vrmsspec(2:end)/sqrt(2); % For non-DC frequencies, rms voltage is amplitude/sqrt(2)
PS = Vrmsspec.^2/R_T; % [W] Power spectrum into resistor of resistance R_T
PSD = PS/df/ENBW; % [W/Hz] Power spectral density into resistor of resistance R_T

%% Plot results
% figure(1);clf;
% semilogx(f,10*log10(PSD*1000));
% xlabel('Frequency [Hz]');
% ylabel(['PSD into ' num2str(R_T) '\Omega resistor [dBm/Hz]']);

% Power = sum(PSD)*df
% Vrms = sqrt(Power*R_T)
end